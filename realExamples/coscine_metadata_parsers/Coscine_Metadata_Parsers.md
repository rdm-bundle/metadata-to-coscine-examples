# Coscine Metadata Parsers

___

Coscine requires metadata forms to be completed for each file uploaded. Uploading files and adding metadata to the AP form by hand is time consuming for researchers. So there is a need to automize this process so researchers are able to quickly and efficiently use Coscine. 

Working together with other data stewards in RWTH IT Center we have started creating Coscine Metadata Parsers. These parsers will all inherit the base class IParser. Iparser consists of just two functions: 

    + parse_file()
    + fill_metadata_form()

The parsers will be imported to a Flask application in order to create a webpage that connects with Coscine. The website will accept a Coscine user token. Then users can select the type of data that they want to upload to Coscine, the project, and the resource where they want the data uploaded. 

Depending on the type of data a parser will run in the backend which automatically extracts the metadata and sends the file and completed metadata form to Coscine.

 