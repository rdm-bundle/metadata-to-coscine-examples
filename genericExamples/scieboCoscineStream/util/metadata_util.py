import json
import pathlib
import datetime
import coscine
from rdflib import Graph
import os


class Metadata:
    def __init__(self, resource) -> None:
        self.resource = resource
        self.metadata_form = resource.metadata_form()

    # this is where you could put in your own parser or specific functions
    def parseMetadataJSON(self, filepath: pathlib.Path | str) -> dict:
        """
        gets metadata from a json file according to a given path
        returns a dictionary of said metadata
        """
        with open(filepath, "r") as f:
            metadata = json.load(f)
        return metadata

    def convertMetadataCoscine(
        self,
        metadata_dict: dict,
        nested: bool = False,
    ) -> coscine.MetadataForm:
        """
        Convert metadata to the coscine form.
        Takes metadata in a dictionary and returns the filled out form.

        takes and optional parameter nested, default is False. This aims to deal
        with more complicated, nested application profiles (WIP)
        """
        if nested:
            # this is very specific to the employed metadata profile!
            # TODO WIP - doesn't work yet!
            metadata_template = f"""
            @prefix dcterms: <http://purl.org/dc/terms/> .
            @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
            @prefix schema: <http://schema.org/> .
            @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
            @prefix aps: <https://purl.org/coscine/ap/> .

            _:b5 a <https://purl.org/coscine/ap/sfb985/archive/> ;
                dcterms:title "{metadata_dict['Title']}" ;
                <http://purl.obolibrary.org/obo/OBI_0000103> <http://purl.org/coscine/vocabularies/sfb985/pis#{self.getVocab("Principal investigator(s)", metadata_dict, purl='<http://purl.org/coscine/vocabularies/sfb985/pis#')}
                <http://www.w3id.org/ecsel-dr-ORG#has_sub_project> <http://purl.org/coscine/vocabularies/sfb985/subProjects#{self.getVocab("SFB 985 sub-projects", metadata_dict,purl='<http://purl.org/coscine/vocabularies/sfb985/subProjects#')}
                dcterms:date "{metadata_dict['Date archived']}"^^xsd:date ;
                <http://edamontology.org/data_0968> "{metadata_dict['Keywords']}" ;
                <http://edamontology.org/data_1084> [
                    rdf:type <https://purl.org/coscine/ap/dataset/> ;
                    dcterms:abstract "{metadata_dict["Research dataset information"]['Description']}" ;
                    <http://purl.org/spar/datacite/ResourceIdentifierScheme> "{metadata_dict['Research dataset information']['Dataset Identifier Type']}" ;
                    <http://purl.org/spar/datacite/ResourceIdentifier> "{metadata_dict['Research dataset information']['Dataset Identifier']}" ;
                    schema:distribution "{metadata_dict['Research dataset information']['Dataset URL']}"^^xsd:anyURI ;
                ] ;
                dcterms:isReferencedBy [
                    rdf:type <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/relatedPublication/> ;
                    <http://purl.org/spar/datacite/ResourceIdentifierScheme> "{metadata_dict["Related published article"]["Related Publication Identifier Type"]}" ;
                    <http://purl.org/spar/datacite/ResourceIdentifier> "{metadata_dict["Related published article"]["Related Publication Identifier"]}" ;
                    schema:distribution "{metadata_dict["Related published article"]["Related Publication URL"]}"^^xsd:anyURI ;
                    dcterms:bibliographicCitation "{metadata_dict["Related published article"]["Related Publication Citation"]}" ;
                ] ;
                dcterms:creator [
                    rdf:type <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/> ;
                    dcterms:creator "{metadata_dict['Creator/Authors']['Author Name']}" ;
                    schema:affiliation "{metadata_dict['Creator/Authors']['Author Affiliation']}" ;
                    <http://purl.org/spar/datacite/AgentIdentifierScheme> "{metadata_dict['Creator/Authors']['Author Identifier Type']}" ;
                    <http://purl.org/spar/datacite/AgentIdentifier> "{metadata_dict['Creator/Authors']['Author Identifier']}" ;
                ] .
            """
            graph_metadata = Graph()
            graph_metadata.parse(data=metadata_template, format="ttl")
            return self.removeEmptyVals(graph_metadata)

        else:
            # this requires your metadata to have the exact same keys as the metadata form
            # and no nested APs
            # add any other data types your AP deals with here!
            for field in self.metadata_form:
                if self.metadata_form.field(field).datatype == datetime.date:
                    date_format = "%Y-%m-%d"
                    date_object = datetime.datetime.strptime(
                        metadata_dict[field], date_format
                    ).date()
                    self.metadata_form[field] = date_object

                else:
                    self.metadata_form[field] = metadata_dict[field]

            return self.metadata_form  # filled form

    def getVocab(self, field_key: str, md: dict, purl=None) -> str:
        """
        Gets the vocab of a coscine metadata field.
        """
        vocab = list(self.metadata_form.field(field_key).vocabulary)
        vocab_index = []
        if isinstance(md[field_key], list):
            for val in md[field_key]:
                vocab_index.append(vocab.index(val))
        else:
            vocab_index.append(vocab.index(md[field_key]))
        return self.convertIndexList(vocab_index, purl)

    def convertIndexList(self, index_list: list, purl) -> str:
        out = str(index_list[0]) + ">"
        if len(index_list) > 1:
            for i, index in enumerate(index_list[1:]):
                out = out + f", {purl}{str(index)}>"
                if i == len(index_list) - 2:

                    out = out + " ;"
        else:
            out = out + " ;"
        return out

    def convertLists(self, md_dict: dict):
        """
        Turn lists in a dictionary into strings.
        """
        for k in md_dict:
            if isinstance(md_dict[k], list):
                md_dict[k] = '", "'.join(md_dict[k])
            if isinstance(md_dict[k], dict):
                md_dict[k] = self.convertLists(md_dict[k])
        return md_dict

    def removeEmptyVals(self, metadata_graph):
        triples_to_remove = []

        # Iterate through triples and identify the ones to remove
        for subj, pred, obj in metadata_graph:
            if str(obj) == "None":
                triples_to_remove.append((subj, pred, obj))

        # Remove the identified triples from the graph
        for triple in triples_to_remove:
            metadata_graph.remove(triple)
        return metadata_graph

    def deleteMetadataFile(self, file_path):
        """
        deletes a local metadata file following a given file_path
        for easy cleanup.

        """
        # Check if the file exists before trying to delete it
        if os.path.exists(file_path):
            os.remove(file_path)
            print(f"File '{file_path}' has been deleted from local system.")
        else:
            print(f"File '{file_path}' does not exist on local system.")
