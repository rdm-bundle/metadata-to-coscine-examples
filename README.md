# Examples for Transferring (Meta)Data to Coscine

This is a collection of generic and real-world example scripts that transfer data and metadata to Coscine.

If you want to add examples, simply add a new folder to this project. You may also contribute to any fitting folders that already exist. Where appropriate, please create a readme in you folder to provide others relevant information. 


**PLEASE NOTE:** These have not necessarily been updated to work with the Coscine APIv2.